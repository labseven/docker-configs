# My selfhosted configs

To run these, change the settings in the run file (put in your api keys and directories for mounting), then either run the `.sh` or use the docker-compose file with `docker-compose up -d`. To update, stop and rm the container `docker stop emby; docker rm emby;` then pull the latest version `docker pull emby/embyserver:latest`, then create a new container `./emby.sh`

The caddy server on my NAS lets me connect to the services th

### Other tools not listed

- Using ZFS zpool + snapshots for backup/data security 
  - Mirrored SSD Cache + Mirrored HDD storage similar to [this setup](https://forum.proxmox.com/threads/zfs-cache-with-mirrored-ssds.84542/)
  - root is on a partition on one of the ssds, gets rsynced regularly, but it takes a few hours to reinstall so not too worried about a drive failure here
- [caddy](https://caddyserver.com/download) has caddyfiles, but I download from their website to get netlify ACME DNS
- I run one radarr server per language, easier to set up that way
- Syncthing to synchronize and backup files. I use Trash Can file versioning on my NAS to protect from accidentally deleting files on my computers. It's scary fast syncing, just putting down my laptop and switching to my desktop the files are already synced and ready to go. Still use git for version control, but when between commits, it's nice to be able to go home and pull up what I was working on from the desktop.
- ssh / sftp / sshfs for accessing files on the nas, but also samba (see samba file for more info)
- I use systemctl for launching some apps on startup
- motion for streaming my webcam. Planning on adding OBS to the pipeline, but it was a mess and I couldn't figure out how to selfhost the stream from OBS.
- [Home Assistant](https://www.home-assistant.io/installation/): I am using the docker supervized version, and it works okay but is annoying because it still uses many local directories. I think using the [HASS OS in a VM](https://www.home-assistant.io/installation/alternative) like virtualbox would be easiest
  - mosquitto for an MQTT server and zigbee2mqtt for bridging all my zigbee devices.
  - [S31](https://www.amazon.com/Sonoff-Monitoring-Certified-Assistant-Supporting/dp/B08GKGS197/ref=sr_1_1?crid=2J88MZEKWIWF1&keywords=s31&qid=1669600806&s=hi&sprefix=s31%2Ctools%2C209&sr=1-1) plugs with [Tasmota](https://tasmota.github.io/docs/) as my primary plug, and IKEA switches/lightbulbs for zigbee lights. Also using the sonoff zigbee motion sensors and temp sensors.
  - Homebrew sensors with the [ESP8266 D1 Mini](https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20221127180127&SearchText=d1+mini&spm=a2g0o.home.1000002.0) running Tasmota
  - [DHT22](https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20221127180244&SearchText=dht22&spm=a2g0o.productlist.1000002.0) for temp/humidity
  - [SCD-30](https://www.adafruit.com/product/4867) for CO2
  - [PMS5003/PMS7003](https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20221127180523&SearchText=pms5003&spm=a2g0o.productlist.1000002.0) for PM2.5 (GET THE CABLE, it's impossible to solder onto)
  - [WS2812](https://www.aliexpress.com/wholesale?catId=0&initiative_id=SB_20221127180831&origin=y&SearchText=WS2812&spm=a2g0o.detail.1000002.0) strips of addressable LEDs (note get a good 12v power supply to run these)
