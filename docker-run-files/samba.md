I use samba for creating shared network drives that can be opened on windows/mac

These can be opened by going to `\\<NAS IP>\<Directory name>` and using the user `nobody` password blank. You can also password protect these with `smbpasswd` ([tutorial](https://www.linuxfordevices.com/tutorials/linux/linux-samba#Installing-Samba-on-Linux))

`sudo apt install samba`

Config file is in `/etc/samba/smb.conf`, after editing run `sudo systemctl restart smbd.service`

```
[global]
workgroup = WORKGROUP
server string = Samba Server %v
netbios name = ubuntu
security = user
map to guest = bad user
name resolve order = bcast host
dns proxy = no
bind interfaces only = yes
guest account = nobody
map to user = bad user

# add folders to the end
[Inbox]
   path = <inbox directory>
   browsable = yes
   writable = yes
   guest ok = yes
   guest only = yes
   read only = no
   create mode = 0777
   directory mode = 0777
   force user = nobody

[Arrg]
   path = <torrent directory>
   browsable = yes
   writable = no
   guest ok = yes
   guest only = yes
   read only = no
   create mode = 0777
   directory mode = 0777
   force user = nobody
```
