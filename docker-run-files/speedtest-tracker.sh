#!/bin/bash
docker run -d --name speedtest-tracker \
    -p 8002:443 \
    -e PUID=1000 -e PGID=1000 \
    --restart=always \
    -v /your/docker/directory/speedtest-tracker:/config \
    ghcr.io/alexjustesen/speedtest-tracker:latest
