to start some containers and other apps. You can also use restart=Always in your docker configs to have these started by the docker server.

`/etc/systemd/system/start_docker.service`

```
[Unit]
Description=Start Assorted Docker containers
Requires=docker.service
After=docker.service

[Service]
#Restart=always
Type=oneshot
ExecStart=/dir/to/docker/configs/start_docker.sh
ExecStop=/dir/to/docker/configs/stop_docker.sh

[Install]
WantedBy=multi-user.target
```

`start_docker.sh`

```
#!/bin/bash
sudo docker start mynodered4 mymosquitto
sudo docker network connect iot mymosquitto
sudo docker network connect iot mynodered4
sudo docker start zigbee2mqtt

sudo docker start transmission emby radarr jackett lidarr
sudo docker start Deemix navidrome

sudo docker start homeassistant

cd ../caddy
sudo bin/caddy start
cd ..
```
