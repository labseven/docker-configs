#!/bin/bash
docker run -d --name Deemix \
              -v /media/directory/music_deemix:/downloads \
              -v /your/docker/directory/deemix:/config \
              -e PUID=1000 \
              -e PGID=1000 \
              -e ARL=<GET ARL FROM DEEZER COOKIES> \
              -e UMASK_SET=022 \
              -e DEEZUI=false \
              -p 8098:6595 \
              registry.gitlab.com/bockiii/deemix-docker
