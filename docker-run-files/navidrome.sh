#!/bin/bash
docker run -d \
   --name navidrome \
   --restart=unless-stopped \
   --user 1000:1000 \
   -v /media/directory/music_deemix:/music \
   -v /your/docker/directory/navidrome:/data \
   -p 8097:4533 \
   -e ND_DEVACTIVITYPANEL=true \
   deluan/navidrome:latest
