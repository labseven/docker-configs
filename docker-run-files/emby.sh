#!/bin/bash
docker run -d \
    --name emby \
    --volume /your/docker/directory/emby/:/config \
    --volume /media/directory/movies:/mnt/shareMovies \
    --volume /media/directory/TV:/mnt/shareTV \
    --volume /your/docker/directory/transmission/completed:/mnt/transmission \
    --volume /your/docker/directory/transmission/TV:/mnt/transmissionTV \
    --net=host \
    --publish 8096:8096 \
    --publish 8920:8920 \
    emby/embyserver:latest
