# Wireguard

I run wireguard on all my devices. All devices connect to my VPS, which then forwards packets to the correct computer, allowing me to access my NAS from any network. This also lets me run caddy on my VPS, and reverse proxy to my NAS over the vpn, easier than poking ports through my router.

I use the official wireguard app on android (download from [f-droid](https://f-droid.org/en/packages/com.wireguard.android/)), mostly to connect to home assistant when out.

Tutorial for running wg is [here](https://blog.fuzzymistborn.com/vps-reverse-proxy-tunnel/). Generate configs [here](https://www.wireguardconfig.com/) or copy those below and create private/public keys with `wg genkey` and `wg pubkey <private key>`

On the VPS, with a peer entry for each client:

```
[Interface]
PrivateKey = <private key>
PostUp = iptables -A FORWARD -i wg0 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i wg0 -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
ListenPort = 51820
Address= 192.168.5.1/24

[Peer]
PublicKey = <public key>
AllowedIPs = 192.168.5.x/32
```

On the other clients:

```
[Interface]
PrivateKey = <priv key>
Address = 192.168.5.42/24

[Peer]
PublicKey = <vps private key>
AllowedIPs = 192.168.5.0/24
Endpoint = <vps domain>
PersistentKeepalive = 25 # optional, I only have this on my NAS to keep the connection from timing out
```
